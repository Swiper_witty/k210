# K210

#### 介绍
在2019年的时候有幸参加了全国电子设计大赛。

当时纯粹划水（大一）

好巧不巧接触OPENMV，看着挺高大上，但是成本昂贵，且需要3d打印支架才能和云台连接。

如果舵机失控openmv必撞连轴。qiudale

于是乎经过漫长的学习（闲的），设计了一个廉价的方案：

装云台wozuimei

还可以装屏幕（装云台一般就不用屏幕了），有点蓝，谁能解决一下。。

写在最后：

这个只需要供电即可，通信用无线就好了（UART）。

我叫卡文迪许怪，请多指教！


#### 使用说明

1.  SIPEED的M1N
2.  Maixpy
3.  ad

#### 效果

![1](https://images.gitee.com/uploads/images/2021/1111/225431_6918c8a4_7821111.jpeg "69388ed75ec9608902a141c17838ba1.jpg")

![2](https://images.gitee.com/uploads/images/2021/1111/225454_adea0974_7821111.jpeg "8837da9d8ef7a91ddacaa56777f9e1d.jpg")

![3](https://images.gitee.com/uploads/images/2021/1111/225656_ba4baa04_7821111.jpeg "e1231f6c9d0dbb8b73ec37b8969af21.jpg")

#### Maixpy 牛逼

[下载链接](https://gitee.com/Swiper_witty/k210)

[B站传送门](https://space.bilibili.com/102898291?spm_id_from=333.1007.0.0)

 :neckbeard: 




