import utime
from Maix import GPIO
from board import board_info
from fpioa_manager import fm

fm.register(12,fm.fpioa.GPIO1)
fm.register(13,fm.fpioa.GPIO2)
fm.register(14,fm.fpioa.GPIO3)

LED_G=GPIO(GPIO.GPIO1,GPIO.OUT)
LED_R=GPIO(GPIO.GPIO2,GPIO.OUT)
LED_B=GPIO(GPIO.GPIO3,GPIO.OUT)

def LED_RGB(A = ' '):
    if A == 'r' or  A == 'R':
        LED_R.value(0)
        LED_G.value(1)
        LED_B.value(1)
    elif A == 'g' or  A == 'G':
        LED_R.value(1)
        LED_G.value(0)
        LED_B.value(1)
    elif A == 'b' or  A == 'B':
        LED_R.value(1)
        LED_G.value(1)
        LED_B.value(0)
    else:
        LED_R.value(1)
        LED_G.value(1)
        LED_B.value(1)
    return

fm.register(22, fm.fpioa.GPIOHS0)
key1_gpio = GPIO(GPIO.GPIOHS0, GPIO.IN)

LED_RGB()
while(1):
    LED_RGB('r')
    utime.sleep_ms(500)
    LED_RGB('g')
    utime.sleep_ms(500)
    LED_RGB('b')
    utime.sleep_ms(500)
